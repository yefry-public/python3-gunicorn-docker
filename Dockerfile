FROM python:3


LABEL MAINTAINER=YefryFigueroa EMAIL=yefry@figueroa.it

#Here I'm installing all the required basic packages
RUN apt-get update && apt-get -y install git htop apache2

#I will use gunicorn as http server
RUN pip install gunicorn


#Setting the desired timezone
ENV TZ=Europe/Rome
RUN ls -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


EXPOSE 8000
